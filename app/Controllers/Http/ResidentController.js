"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Resident = use("App/Models/Resident");
const Apartment = use("App/Models/Apartment");
/**
 * Resourceful controller for interacting with residents
 */
class ResidentController {
  /**
   * Show a list of all residents.
   * GET residents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index() {
    const residents = await Resident.all();

    return residents;
  }

  /**
   *
   * @param {object} ctx
   */
  async getResindentsByApartmentId({ params }) {
    const residents = await this.checkIfResidentsExists(params.id);
    const apartment = await Apartment.find(params.id);
    return { ...apartment.toJSON(), residents };
  }

  /**
   * Create/save a new resident.
   * POST residents
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.only([
      "username",
      "birth",
      "phone",
      "document",
      "email",
      "is_responsible",
      "apartment_id",
    ]);

    const resident = { owner_id: auth.user.id, ...data };

    if (data.is_responsible === true) {
      const existingResidents = await this.checkIfResidentsExists(
        data.apartment_id
      );

      if (existingResidents.length > 0) {
        const hasResponsible = this.checkIfExistsResponsible(existingResidents);

        if (hasResponsible) {
          await Resident.query().where("id", hasResponsible.id).update({
            is_responsible: false,
          });
        }
        return this.saveResident(resident);
      }

      return this.saveResident(resident);
    }

    return this.saveResident(resident);
  }

  /**
   * Check if exists another resident in the same apartment.
   * @param {*} apartment_id
   */
  async checkIfResidentsExists(apartment_id) {
    const residents = await Resident.query()
      .where("apartment_id", "=", apartment_id)
      .fetch();
    return residents.toJSON();
  }

  /**
   * Checki if exists the responsible resident.
   * @param {*} existingResidents
   */
  checkIfExistsResponsible(existingResidents) {
    return existingResidents.find(
      (resident) => resident.is_responsible === true
    );
  }

  /**
   * Save the new resident and checks if the new user is the responsible by the apartment.
   * @param {*} resident
   */
  async saveResident(resident) {
    try {
      const savedResident = await Resident.create(resident);
      if (resident.is_responsible) {
        this.handleapartmentHasResponsible(resident.apartment_id, true);
      }

      return savedResident;
    } catch (error) {
      return "Erro ao salvar morador";
    }
  }

  // /**
  //  * Sets the row in the apartment to true.
  //  * @param {Int16Array} apartment_id
  //  * @param {Boolean} is_responsible
  //  */
  async handleapartmentHasResponsible(apartment_id, is_responsible) {
    const apartment = await Apartment.find(apartment_id);
    apartment.merge({ has_responsible: is_responsible });
    await apartment.save();
  }

  /**
   * Display a single resident.
   * GET residents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params }) {
    const resident = await Resident.find(params.id);
    let apartments = await Apartment.all();
    if (!resident) return "Resident inexistente";
    apartments.toJSON();

    return { resident, apartments };
  }

  /**
   * Update resident details.
   * PUT or PATCH residents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      "username",
      "birth",
      "phone",
      "is_responsible",
      "apartment_id",
    ]);

    const resident = { id: params.id, ...data };

    if (data.is_responsible === true) {
      const existingResidents = await this.checkIfResidentsExists(
        data.apartment_id
      );

      if (existingResidents.length > 0) {
        const hasResponsible = this.checkIfExistsResponsible(existingResidents);
        if (hasResponsible && hasResponsible.id !== resident.id) {
          await Resident.query().where("id", hasResponsible.id).update({
            is_responsible: false,
          });
        }
        return this.updateResident(resident);
      }
      return this.updateResident(resident);
    }

    return this.updateResident(resident);
  }

  /**
   * Update de resident and checks if he was the responsible, if yes,
   * it changes the apartments row has_responsible to false
   * @param {*} resident
   */
  async updateResident(residentToBeUpdate) {
    const oldResidentData = await Resident.find(residentToBeUpdate.id);

    await this.updateApartment({ oldResidentData: oldResidentData.toJSON(), residentToBeUpdate });

    oldResidentData.merge({ ...residentToBeUpdate });
    const updatedResident = await oldResidentData.save();

    return updatedResident;
  }

  async updateApartment({ oldResidentData, residentToBeUpdate }) {
    
    const userHasChangedApartment = residentToBeUpdate.apartment_id === oldResidentData.apartment_id ? false : true;
    if (!userHasChangedApartment) {
      
      const apartmentData = await Apartment.find(residentToBeUpdate.apartment_id);
      const apartment = apartmentData.toJSON();
      
      const userWasResponsible =  oldResidentData.is_responsible;
      const userIsResponsibleNow  =  residentToBeUpdate.is_responsible;
      const apartmentHasResponsible = apartment.has_responsible;

      if(userWasResponsible &&  !userIsResponsibleNow) {
        apartmentData.merge({ has_responsible: false });
        await apartmentData.save();
      } else if (userIsResponsibleNow &&  !apartmentHasResponsible) {
        apartmentData.merge({ has_responsible: true });
        await apartmentData.save();
      }

    } else {
      
      const userWasResponsibleInOldApartment = oldResidentData.is_responsible;
      const userIsResponsibleInNewApartment = residentToBeUpdate.is_responsible;

      if (userWasResponsibleInOldApartment) {
        
        if (userIsResponsibleInNewApartment) {
          const newApartmentData = await Apartment.find(residentToBeUpdate.apartment_id);
          const newApartment = newApartmentData.toJSON();
          
          if (!newApartment.has_responsible) {
            newApartmentData.merge({ has_responsible: true });
            newApartmentData.save();
          }
        }

        const oldApartment = await Apartment.find(oldResidentData.apartment_id);
        oldApartment.merge({ has_responsible: false });
        await oldApartment.save();
      }
    }
  }

  /**
   * Delete a resident with id.
   * DELETE residents/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params }) {
    const resident = await Resident.findOrFail(params.id);
    await this.handleapartmentHasResponsible(resident.apartment_id, false);
    await resident.delete();
  }
}

module.exports = ResidentController;
