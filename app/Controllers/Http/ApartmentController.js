"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */

const Apartment = use("App/Models/Apartment");

/**
 * Resourceful controller for interacting with apartments
 */
class ApartmentController {
  /**
   * Show a list of all apartments.
   * GET apartments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index() {
    const apartments = await Apartment.all();

    return apartments;
  }

  /**
   * Create/save a new apartment.
   * POST apartments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.only(["number", "block"]);
    const isNumberExists = await Apartment.findBy("number", data.number);

    if (isNumberExists) {
      return "Já existe apartamento com esse número";
    }
    const apartment = await Apartment.create({
      owner_id: auth.user.id,
      has_responsible: false,
      ...data,
    });
    return apartment;
  }

  /**
   * Display a single apartment.
   * GET apartments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params }) {
    const apartment = await Apartment.find(params.id);
    if (!apartment) return "Apartamento inexistente";
    return apartment;
  }

  /**
   * Update apartment details.
   * PUT or PATCH apartments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["block", "number"]);
    const apartment = await Apartment.find(params.id);

    apartment.merge({ ...data });
    await apartment.save();
    return apartment;
  }

  /**
   * Delete a apartment with id.
   * DELETE apartments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params }) {
    const apartment = await Apartment.findOrFail(params.id);
    await apartment.delete();
  }
}

module.exports = ApartmentController;
