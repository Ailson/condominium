"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ResidentSchema extends Schema {
  up() {
    this.create("residents", (table) => {
      table.increments();
      table.string("username").notNullable();
      table.date("birth").notNullable();
      table.string("phone").notNullable();
      table.string("document").notNullable().unique();
      table.string("email").notNullable().unique();
      table.boolean("is_responsible").notNullable();
      table
        .integer("apartment_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("apartments")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer("owner_id")
        .notNullable()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.timestamps();
    });
  }

  down() {
    this.drop("residents");
  }
}

module.exports = ResidentSchema;
