"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ApartmentSchema extends Schema {
  up() {
    this.create("apartments", (table) => {
      table.increments();
      table.integer("number").notNullable().unique();
      table.string("block").notNullable();
      table.boolean("has_responsible").notNullable();
      table
        .integer("owner_id")
        .notNullable()
        .references("id")
        .inTable("users")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.timestamps();
    });
  }

  down() {
    this.drop("apartments");
  }
}

module.exports = ApartmentSchema;
